package br.com.globo.integrator.domain.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.globo.integrator.domain.model.DadosPrograma;
import br.com.globo.integrator.domain.repository.DadosProgramaRepository;

@Service
public class DadosProgramaDAO {

	@Autowired
	DadosProgramaRepository programaRepository;

	public DadosPrograma salvarPrograma(DadosPrograma estado) {
		DadosPrograma programaSalvo = null;
		try {

			programaSalvo = programaRepository.save(estado);
			
		} catch (Exception e) {
			System.err.println("Erro ao salvar programa. " + e.getMessage());
			e.printStackTrace();
			programaSalvo =null;
		}
		return programaSalvo;
	}

	public Boolean isExist(int code) {
		boolean exist = false;
		DadosPrograma programa = null;
		try {
			programa = programaRepository.isExist(code);
			if (programa != null) {
				exist = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return exist;

	}
	
	public Long findByName(String name) {
		@SuppressWarnings("deprecation")
		Long retorno=new Long(0);
		try {
			retorno = programaRepository.findByName(name);
		} catch (Exception e) {
			System.err.println("Erro ao buscar programa por nome. " + e.getMessage());
		}
		return retorno;
	}

}

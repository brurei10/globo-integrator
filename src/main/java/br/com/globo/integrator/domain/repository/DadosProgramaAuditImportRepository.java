package br.com.globo.integrator.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.globo.integrator.domain.model.audit.DadosProgramaAudit;
import br.com.globo.integrator.domain.model.audit.DadosProgramaAuditImport;


@Repository
public interface DadosProgramaAuditImportRepository extends CrudRepository<DadosProgramaAuditImport, Long>,  JpaRepository<DadosProgramaAuditImport, Long> {

	@Query(value = "SELECT * FROM dados_programa_audit_import WHERE id =?1", nativeQuery = true)
	DadosProgramaAuditImport isExist(int id);
	
	@Query(value = "SELECT id FROM dados_programa_audit_import WHERE nome_programa =?1", nativeQuery = true)
	Long findByName(String name);

}

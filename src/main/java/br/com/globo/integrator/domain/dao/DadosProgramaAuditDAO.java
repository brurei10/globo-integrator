package br.com.globo.integrator.domain.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.globo.integrator.domain.model.DadosPrograma;
import br.com.globo.integrator.domain.model.audit.DadosProgramaAudit;
import br.com.globo.integrator.domain.model.audit.DadosProgramaAuditImport;
import br.com.globo.integrator.domain.repository.DadosProgramaAuditRepository;

@Service
public class DadosProgramaAuditDAO {

	@Autowired
	DadosProgramaAuditRepository programaRepository;

	public DadosProgramaAudit salvarPrograma(DadosProgramaAudit program) {
		DadosProgramaAudit programaSalvo = null;
		try {

			programaSalvo = programaRepository.save(program);
			
		} catch (Exception e) {
			System.err.println("Erro ao salvar programa. " + e.getMessage());
			e.printStackTrace();
			programaSalvo =null;
		}
		return programaSalvo;
	}
	
	
	public DadosProgramaAuditImport convertToJson(DadosPrograma program) {
		DadosProgramaAuditImport programaSalvo = new DadosProgramaAuditImport();
		try {

			programaSalvo.setBuscarPrecoFaixaHoraria(program.getBuscarPrecoFaixaHoraria());
			programaSalvo.setCustoEspecial(program.getCustoEspecial());
			programaSalvo.setDataFimVigencia(program.getDataFimVigencia());
			programaSalvo.setDataInicioVigencia(program.getDataInicioVigencia());
			programaSalvo.setHoraInicio(program.getHoraInicio());
			programaSalvo.setIdCanalExibicao(program.getIdCanalExibicao());
			programaSalvo.setIdFaixaReferenciaTabPreco(program.getIdFaixaReferenciaTabPreco());
			programaSalvo.setIdPrograma(program.getIdPrograma());
			programaSalvo.setIdProgramaReferenciaTabPreco(program.getIdProgramaReferenciaTabPreco());
			programaSalvo.setNomePrograma(program.getNomePrograma());
			programaSalvo.setPayloadRequest("");
			programaSalvo.setPayloadResponse("");
			
		} catch (Exception e) {
			
			System.err.println("Erro ao converter programa. " + e.getMessage());
			e.printStackTrace();
			programaSalvo =null;
		}
		
		return programaSalvo;
	}

	public Boolean isExist(int code) {
		boolean exist = false;
		DadosProgramaAudit programa = null;
		try {
			programa = programaRepository.isExist(code);
			if (programa != null) {
				exist = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return exist;

	}
	
	public Long findByName(String name) {
		@SuppressWarnings("deprecation")
		Long retorno=new Long(0);
		try {
			retorno = programaRepository.findByName(name);
		} catch (Exception e) {
			System.err.println("Erro ao buscar programa por nome. " + e.getMessage());
		}
		return retorno;
	}

}

package br.com.globo.integrator.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.globo.integrator.domain.model.DadosPrograma;


@Repository
public interface DadosProgramaRepository extends CrudRepository<DadosPrograma, Long>,  JpaRepository<DadosPrograma, Long> {

	@Query(value = "SELECT * FROM dados_programa WHERE id =?1", nativeQuery = true)
	DadosPrograma isExist(int id);
	
	@Query(value = "SELECT id FROM dados_programa WHERE nome_programa =?1", nativeQuery = true)
	Long findByName(String name);

}

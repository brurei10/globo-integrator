package br.com.globo.integrator.domain.model.audit;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table
@AuditTable(value = "dados_programa_audited")
public class DadosProgramaAudit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "id_programa", nullable = false)
	private Long idPrograma; 
	
	@Column(name = "sigla_programa", nullable = false)
	private String siglaPrograma;
	
	@Column(name = "nome_programa", nullable = false)
	private String nomePrograma;
	
	@Column(name = "id_canal_exibicao", nullable = false)
	private Long idCanalExibicao;
	
	@Column(name = "tipo_evento_exibicao", nullable = false)
	private Long tipoEventoExibicao;
	
	@Column(name = "data_inicio_vigencia", nullable = false)
	private String dataInicioVigencia;
	
	@Column(name = "data_fim_vigencia", nullable = false)
	private String dataFimVigencia;
	
	@Column(name = "hora_inicio", nullable = false)
	private Long horaInicio;
	
	@Column(name = "id_programa_referencia_tab_preco", nullable = false)
	private Long idProgramaReferenciaTabPreco;
	
	@Column(name = "busca_preco_faixa_horaria", nullable = false)
	private char buscarPrecoFaixaHoraria;
	
	@Column(name = "custo_especial", nullable = false)
	private char custoEspecial;
	
	@Column(name = "id_faixa_referencia_tab_preco", nullable = false)
	private Long idFaixaReferenciaTabPreco;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPrograma() {
		return idPrograma;
	}

	public void setIdPrograma(Long idPrograma) {
		this.idPrograma = idPrograma;
	}

	public String getSiglaPrograma() {
		return siglaPrograma;
	}

	public void setSiglaPrograma(String siglaPrograma) {
		this.siglaPrograma = siglaPrograma;
	}

	public String getNomePrograma() {
		return nomePrograma;
	}

	public void setNomePrograma(String nomePrograma) {
		this.nomePrograma = nomePrograma;
	}

	public Long getIdCanalExibicao() {
		return idCanalExibicao;
	}

	public void setIdCanalExibicao(Long idCanalExibicao) {
		this.idCanalExibicao = idCanalExibicao;
	}

	public Long getTipoEventoExibicao() {
		return tipoEventoExibicao;
	}

	public void setTipoEventoExibicao(Long tipoEventoExibicao) {
		this.tipoEventoExibicao = tipoEventoExibicao;
	}

	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public String getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public Long getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Long horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Long getIdProgramaReferenciaTabPreco() {
		return idProgramaReferenciaTabPreco;
	}

	public void setIdProgramaReferenciaTabPreco(Long idProgramaReferenciaTabPreco) {
		this.idProgramaReferenciaTabPreco = idProgramaReferenciaTabPreco;
	}

	public char getBuscarPrecoFaixaHoraria() {
		return buscarPrecoFaixaHoraria;
	}

	public void setBuscarPrecoFaixaHoraria(char buscarPrecoFaixaHoraria) {
		this.buscarPrecoFaixaHoraria = buscarPrecoFaixaHoraria;
	}

	public char getCustoEspecial() {
		return custoEspecial;
	}

	public void setCustoEspecial(char custoEspecial) {
		this.custoEspecial = custoEspecial;
	}

	public Long getIdFaixaReferenciaTabPreco() {
		return idFaixaReferenciaTabPreco;
	}

	public void setIdFaixaReferenciaTabPreco(Long idFaixaReferenciaTabPreco) {
		this.idFaixaReferenciaTabPreco = idFaixaReferenciaTabPreco;
	}
	
	
	
	
}

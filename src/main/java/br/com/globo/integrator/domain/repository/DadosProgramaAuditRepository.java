package br.com.globo.integrator.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.globo.integrator.domain.model.audit.DadosProgramaAudit;


@Repository
public interface DadosProgramaAuditRepository extends CrudRepository<DadosProgramaAudit, Long>,  JpaRepository<DadosProgramaAudit, Long> {

	@Query(value = "SELECT * FROM dados_programa_audited WHERE id =?1", nativeQuery = true)
	DadosProgramaAudit isExist(int id);
	
	@Query(value = "SELECT id FROM dados_programa_audited WHERE nome_programa =?1", nativeQuery = true)
	Long findByName(String name);

}

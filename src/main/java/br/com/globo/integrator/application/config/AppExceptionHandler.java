package br.com.globo.integrator.application.config;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.globo.integrator.application.exception.BusinessException;
import br.com.globo.integrator.application.exception.ConflictException;
import br.com.globo.integrator.application.i18n.Message;
import br.com.globo.integrator.application.i18n.MessageProperties;
import br.com.globo.integrator.infrastructure.response.ErrorData;
import br.com.globo.integrator.infrastructure.response.ErrorResponse;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	Message messages;

	private static final Logger logger = LoggerFactory.getLogger(AppExceptionHandler.class);

	@Override
	protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorResponse body = new ErrorResponse(messages.get(MessageProperties.API_FIELDS_INVALID), ex.getMessage(), null);
		return handleExceptionInternal(ex, body, headers, HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorResponse body = new ErrorResponse(messages.get(MessageProperties.API_UNKNOWN_FIELDS), ex.getMessage(), null);
		return handleExceptionInternal(ex, body, headers, HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ErrorResponse body = new ErrorResponse(messages.get(MessageProperties.API_FIELDS_INVALID), ex.getMessage());
		body.setErrors(populateErrors(body, ex.getBindingResult()));
		return handleExceptionInternal(ex, body, headers, status, request);
	}
	
	@ExceptionHandler(value = { BusinessException.class })
	protected ResponseEntity<Object> handleBusinessException(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.BAD_REQUEST;
		ErrorResponse body = new ErrorResponse(messages.get(ex.getMessage()), ex.getMessage(), null);
		return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
	}
	@ExceptionHandler(value = { ConflictException.class })
	protected ResponseEntity<Object> handleConflictException(ConflictException ex, WebRequest request) {
		HttpStatus status = HttpStatus.CONFLICT;
		ErrorResponse body = new ErrorResponse(messages.get(ex.getMessage()), ex.getMessage(), null);
		body.setData(ex.getResponse());
		return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
	}
	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Object> handleInternalServerErrorException(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		ErrorResponse body = new ErrorResponse(messages.get(MessageProperties.INTERNAL_ERROR), ex.getMessage(), null);
		logError(ex);

		return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
	}
	
	@ExceptionHandler(value = { EntityNotFoundException.class })
	public ResponseEntity<Object> handleConstraintViolation(EntityNotFoundException ex, WebRequest request) {
		ErrorResponse body = new ErrorResponse(messages.get(MessageProperties.ENTITY_NOT_FOUND), ex.getMessage(), null);

		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	@ExceptionHandler(value = { ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		ErrorResponse body = new ErrorResponse(messages.get(MessageProperties.API_FIELDS_INVALID), ex.getMessage());

		//populateErrors(body, ex.getConstraintViolations());
		return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	public List<ErrorData> populateErrors(ErrorResponse response, BindingResult bindingResult) {
		List<ErrorData> errorsList = new ArrayList<ErrorData>();

		for (FieldError fieldError : bindingResult.getFieldErrors()) {
			ErrorData error = new ErrorData(fieldError.getField(), fieldError.toString(), messages.get(fieldError), fieldError.getRejectedValue());
			errorsList.add(error);
		}
		response.setErrors(errorsList);

		return errorsList;
	}

	public List<ErrorData> populateErrors(ErrorResponse response, Set<ConstraintViolationException> errors) {
		List<ErrorData> errorsList = new ArrayList<ErrorData>();
		
		/*
		for (ConstraintViolation<?> fieldError : errors) {
			ConstraintViolation<?> violation = errors.iterator().next();
			// get the last node of the violation
			String field = null;
			for (Node node : violation.getPropertyPath()) {
				field = node.getName();
			}
			String message = MessageFormat.format(fieldError.getMessage(), field);
			ErrorData error = new ErrorData(field, message, message, fieldError.getInvalidValue());
			errorsList.add(error);
		}*/
		response.setErrors(errorsList);

		return errorsList;
	}

	private void logError(Exception ex) {
		logger.error(ex.getMessage());
		logger.error(ex.getLocalizedMessage());
		ex.printStackTrace();
	}
}

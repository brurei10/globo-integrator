package br.com.globo.integrator.application.exception;

import br.com.globo.integrator.application.i18n.MessageProperties;

public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = -6281552239710264585L;

	public BusinessException(String message) {
		super(message);
	}
	public BusinessException(MessageProperties prop) {
		super(prop.toString());
	}
	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

}

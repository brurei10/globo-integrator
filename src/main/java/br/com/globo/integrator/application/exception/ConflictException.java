package br.com.globo.integrator.application.exception;

import br.com.globo.integrator.application.i18n.MessageProperties;

public class ConflictException extends RuntimeException {

	Object response;
	
	private static final long serialVersionUID = -3330500063958333805L;
	
	public ConflictException(String message) {
		super(message);
	}
	public ConflictException(MessageProperties prop) {
		super(prop.toString());
	}
	public ConflictException(MessageProperties prop, Object response) {
		super(prop.toString());
		this.response = response;
	}
	public ConflictException(String message, Throwable cause) {
		super(message, cause);
	}
	public Object getResponse() {
		return response;
	}
	public void setResponse(Object response) {
		this.response = response;
	}
}

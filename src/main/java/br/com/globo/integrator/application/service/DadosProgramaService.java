package br.com.globo.integrator.application.service;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.globo.integrator.application.i18n.Message;
import br.com.globo.integrator.application.i18n.MessageProperties;
import br.com.globo.integrator.domain.dao.DadosProgramaAuditDAO;
import br.com.globo.integrator.domain.dao.DadosProgramaDAO;
import br.com.globo.integrator.domain.model.DadosPrograma;
import br.com.globo.integrator.domain.repository.DadosProgramaAuditImportRepository;
import br.com.globo.integrator.domain.repository.DadosProgramaRepository;
import br.com.globo.integrator.infrastructure.response.ApiResponse;
import br.com.globo.integrator.infrastructure.response.ResponseEntityUtil;
import br.com.globo.integrator.infrastructure.util.NullValidator;
import br.com.globo.integrator.infrastructure.util.TextPadronize;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
public class DadosProgramaService {

	@Autowired
	DadosProgramaDAO programaDAO;

	@Autowired
	DadosProgramaAuditDAO programaAuditDAO;

	@Autowired
	DadosProgramaRepository programaRepository;

	@Autowired
	DadosProgramaAuditImportRepository programaImportRepository;

	@Autowired
	Message messages;

	@Autowired
	NullValidator validateNull;

	@ApiOperation(value = "Efetua o cadastro do programa para o vínculo com o sis.com.")
	@ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Possíveis retornos: programa cadastrado com sucesso. / programa já cadastrado."),
			@io.swagger.annotations.ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
			@io.swagger.annotations.ApiResponse(code = 500, message = "Erro interno. Por favor, contate o nosso suporte."),
			@io.swagger.annotations.ApiResponse(code = 422, message = "Possíveis retornos: Nome do programa Obrigatório. / Erro nos campos enviados. Verifique os valores e nomes dos campos."),

	})

	@PostMapping(value = "/create_program", produces = "application/json", consumes = "application/json")
	@ResponseBody
	@CrossOrigin
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> cadastrarprograma(@RequestBody DadosPrograma programa) {

		try {
			programa.setNomePrograma(programa.getNomePrograma().trim().toUpperCase());
			if (validateNull.isNotNull(programa.getNomePrograma()) || programa.getNomePrograma().equalsIgnoreCase("")) {
				try {
					Long isExist = programaDAO.findByName(programa.getNomePrograma());

					if (validateNull.isNotNull(isExist)) {
						return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.DUPLICATE_PROGRAM));
					} else {

						programaDAO.salvarPrograma(programa);
						return ResponseEntityUtil.createdResponseEntity(messages.get(MessageProperties.PROGRAM_CREATED),
								programa);
					}
				} catch (Exception e) {
					e.printStackTrace();
					return ResponseEntityUtil
							.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR), null);

				}

			} else {

				return ResponseEntityUtil.unprocessableResponseEntity(
						messages.get(MessageProperties.PROGRAM_NAME_REQUIRED), programa.getNomePrograma());
			}

		} catch (Exception e) {
			return ResponseEntityUtil.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR),
					messages.get(MessageProperties.API_FIELDS_INVALID));
		}
	}

	@PostMapping(value = "/send_program", produces = "application/json", consumes = "application/json")
	@ResponseBody
	@CrossOrigin
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> sendProgram(@RequestBody DadosPrograma programa) {

		try {
			programa.setNomePrograma(programa.getNomePrograma().trim().toUpperCase());
			if (validateNull.isNotNull(programa.getNomePrograma()) || programa.getNomePrograma().equalsIgnoreCase("")) {
				try {
					Long isExist = programaDAO.findByName(programa.getNomePrograma());

					if (validateNull.isNotNull(isExist)) {
						return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.DUPLICATE_PROGRAM));
					} else {

						programaImportRepository.saveAndFlush(programaAuditDAO.convertToJson(programa));
						return ResponseEntityUtil.createdResponseEntity(messages.get(MessageProperties.PROGRAM_CREATED),
								programa);
					}
				} catch (Exception e) {
					e.printStackTrace();
					return ResponseEntityUtil
							.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR), null);

				}

			} else {

				return ResponseEntityUtil.unprocessableResponseEntity(
						messages.get(MessageProperties.PROGRAM_NAME_REQUIRED), programa.getNomePrograma());
			}

		} catch (Exception e) {
			return ResponseEntityUtil.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR),
					messages.get(MessageProperties.API_FIELDS_INVALID));
		}
	}

	@ApiOperation(value = "Efetua a consulta da lista de programas. Retorna os dados do programa. Ex http://localhost:8080/get_all_program ")
	@ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Possíveis retornos: programa não encontrada. / Resposta enviada com sucesso (Dados do programa)."),
			@io.swagger.annotations.ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
			@io.swagger.annotations.ApiResponse(code = 500, message = "Erro interno. Por favor, contate o nosso suporte."),
			@io.swagger.annotations.ApiResponse(code = 422, message = "Possíveis retornos: Nome do programa Obrigatório. / Erro nos campos enviados. Verifique os valores e nomes dos campos."),

	})
	@GetMapping(value = "/get_all_program", produces = "application/json")
	@ResponseBody
	@CrossOrigin
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getprogramaNome(@RequestParam("nome") String nome) {
		List<DadosPrograma> programa = null;
		try {
			programa = programaRepository.findAll();
			if (!programa.isEmpty()) {
				return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.PROGRAM_NOTFOUND));
			} else {
				return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.API_SUCCESS));

			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntityUtil.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR),
					messages.get(MessageProperties.API_UNKNOWN_FIELDS));
		}
	}

}

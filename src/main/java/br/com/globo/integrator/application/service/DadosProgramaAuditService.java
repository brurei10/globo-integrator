package br.com.globo.integrator.application.service;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.globo.integrator.application.i18n.Message;
import br.com.globo.integrator.application.i18n.MessageProperties;
import br.com.globo.integrator.domain.dao.DadosProgramaAuditDAO;
import br.com.globo.integrator.domain.dao.DadosProgramaDAO;
import br.com.globo.integrator.domain.model.DadosPrograma;
import br.com.globo.integrator.domain.model.audit.DadosProgramaAudit;
import br.com.globo.integrator.domain.model.audit.DadosProgramaAuditImport;
import br.com.globo.integrator.domain.repository.DadosProgramaAuditImportRepository;
import br.com.globo.integrator.domain.repository.DadosProgramaAuditRepository;
import br.com.globo.integrator.domain.repository.DadosProgramaRepository;
import br.com.globo.integrator.infrastructure.response.ApiResponse;
import br.com.globo.integrator.infrastructure.response.ResponseEntityUtil;
import br.com.globo.integrator.infrastructure.util.NullValidator;
import br.com.globo.integrator.infrastructure.util.TextPadronize;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin
public class DadosProgramaAuditService {

	@Autowired
	DadosProgramaAuditDAO programaDAO;

	@Autowired
	DadosProgramaAuditRepository programaRepository;
	
	@Autowired
	DadosProgramaAuditImportRepository programaImportRepository;
	
	@Autowired
	Message messages;

	@Autowired
	NullValidator validateNull;

	
	@ApiOperation(value = "Efetua a consulta da lista de auditoria dos programas importados. Retorna os dados do programa. Ex http://localhost:8080/get-audit ")
	@ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Possíveis retornos: programa não encontrada. / Resposta enviada com sucesso (Dados do programa)."),
			@io.swagger.annotations.ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
			@io.swagger.annotations.ApiResponse(code = 500, message = "Erro interno. Por favor, contate o nosso suporte."),
			@io.swagger.annotations.ApiResponse(code = 422, message = "Possíveis retornos: Nome do programa Obrigatório. / Erro nos campos enviados. Verifique os valores e nomes dos campos."),
			
		})
	@GetMapping(value = "/get-audit", produces="application/json")
	@ResponseBody
	@CrossOrigin
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getProgram() {
		List<DadosProgramaAudit> programa = null;
		try {
			programa = programaRepository.findAll();
			if (!programa.isEmpty()) {
				return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.PROGRAM_NOTFOUND));
			} else {
				return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.API_SUCCESS));
						
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntityUtil.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR),
					messages.get(MessageProperties.API_UNKNOWN_FIELDS));
		}
	}
	
	@ApiOperation(value = "Efetua a consulta da lista de auditoria dos programas importados com seus payloads de envio e resposta. Retorna os dados do programa. Ex http://localhost:8080/get-audit-import ")
	@ApiResponses(value = {
			@io.swagger.annotations.ApiResponse(code = 200, message = "Possíveis retornos: programa não encontrada. / Resposta enviada com sucesso (Dados do programa)."),
			@io.swagger.annotations.ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
			@io.swagger.annotations.ApiResponse(code = 500, message = "Erro interno. Por favor, contate o nosso suporte."),
			@io.swagger.annotations.ApiResponse(code = 422, message = "Possíveis retornos: Nome do programa Obrigatório. / Erro nos campos enviados. Verifique os valores e nomes dos campos."),
			
		})
	@GetMapping(value = "/get-audit-import", produces="application/json")
	@ResponseBody
	@CrossOrigin
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponse> getProgramImport() {
		List<DadosProgramaAuditImport> programa = null;
		try {
			programa = programaImportRepository.findAll();
			if (!programa.isEmpty()) {
				return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.PROGRAM_NOTFOUND));
			} else {
				return ResponseEntityUtil.okResponseEntity(messages.get(MessageProperties.API_SUCCESS));
						
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntityUtil.unprocessableResponseEntity(messages.get(MessageProperties.INTERNAL_ERROR),
					messages.get(MessageProperties.API_UNKNOWN_FIELDS));
		}
	}



}

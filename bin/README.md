# Services Compasso

[![N|Solid](https://www.uoldiveo.com.br/wp-content/uploads/2020/02/LogoCompasso-e1581697958736.png)](https://www.uoldiveo.com.br/wp-content/uploads/2020/02/LogoCompasso-e1581697958736.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Sistema responsável pela liberaçãoo de microserviços para integraçãoo com aplicações que queiram cadastrar Clientes vinculados as suas Cidades e Estados.

  - Liberação de serviços para gestão e controle de clientes.
  
# New Features!

- Cadastrar cidade
- Cadastrar cliente
- Consultar cidade pelo nome
- Consultar cidade pelo estado
- Consultar cliente pelo nome
- Consultar cliente pelo Id
 - Remover cliente
- Alterar o nome do cliente
COMO PLUS:
 - Disponibilização do Swagger para auxiliar no uso e entendimento das regras do negócio;
 - Possibilidade de alteração não apenas do nome do cliente porém dos outros dados;
 - Cadastro de estados;
 - Pré cadastro dos estados ao subir a aplicação 

### Tech

Tecnologias, frameworks e componentes utilizados:

* [Java 8] - Versão do java para compilação do sistema
* [Spring Framework] Utilização de alguns dos componentes spring para agilidade no desenvolvimento
* [Postman] - Utilizado para testes dos endpoints criados. Para import dos endpoints, basta exportar os mesmos do swagger para arquivo do tipo json.
* [Spring Boot] - Para otimização do trabalho e foco na regra de negócio
* [Tomcat Embeded] - Servidor web embarcado para fácil build e execulção do projeto.
* [H2/Mysql] - Bancos de dados testados na aplicação. H2 facilita o uso devido a utilização em memória, porém os dados serão perdidos a cada novo rum. Mysql é um dos bancos relacionais mais utilizados e free. Foi testado na versão 5 e 8.
* [maven] - para gestão de dependências
* [Swagger] - Para documentação dos serviços criados
* [Factory e MVC] - Para generalização e centralização dos códigos comuns assim como separação de regras de negócio, modelos, serviços, controladores e ultilitários
* [GIT] - Para gestão dos fontes do projeto

### Installation

Clonar o projeto de [Clone](https://gitlab.com/brurei10/compasso-services.git).

Incorporar o projeto em seu IDE de preferencia (Eclipse, Netbeans, Intellj)

Após baixar o projeto e o mesmo estar configurado como 'boot' efetue as seguintes etapas:
- Caso não esteja habilitado como projeto  maven, habilite-o.
Executar os comandos do Maven:
- Maven Clean
- Maven Install
- E, caso não possua o plugin do spring boot, favor baixar e instalar;
- Antes de subir a aplicação, deve-se selecionar qual banco de dados será imcorporado, por padrão está no H2, porém, para alterar basta acessar o arquivo: application.properties. Nele você verá uma parte comentada que é a configuração do banco Mysql, para usar, basta descomentar o trecho e comentar a configuração do H2 ou vice-versa assim como ajustar as credenciais de autenticação da sua base de dados.
- Após este passo, executar o projeto como ----> - spring boot Aplication:
- Caso seja eclipse: Botão direito no projeto > Run as > Spring boot application.
- Aguardar o projeto subir. Não é preciso que você crie a base de dados. Ao alterar a configuração de qual banco usar, o sistema irá criar as tabelas assim como criar o cadastro de todos os estados para facilitar o uso. O arquivo com os scripts chama-se * data.sql
- para verificar se o sistema está no ar, basta acessar: http://localhost:8080
- Para utilização e teste dos endpoints, podem ser feitos pelo navegador ou pelo Postman.
- Para visualização dos endpoints e instruções de uso, basta acessar o swagger:
http://localhost:8080/swagger-ui.html
- Caso esteja usando o BD H2, segue dados para acesso:
- http://localhost:8080/h2-console/, JDBC URL= jdbc:h2:mem:compasso, User=sa, Senha=
- Obrigado e bons testes!